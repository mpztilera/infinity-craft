package anvil.infinity.abilities;


import net.minecraft.entity.EntityLivingBase;

public class AbilityHasPowerStone extends AbilityHasStone {

    public AbilityHasPowerStone(EntityLivingBase entity) {
        super(entity);
    }
}
