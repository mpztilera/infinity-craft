package anvil.infinity.abilities;

import net.minecraft.entity.EntityLivingBase;

public class AbilityHasMindStone extends AbilityHasStone {

    public AbilityHasMindStone(EntityLivingBase entity) {
        super(entity);
    }
}
