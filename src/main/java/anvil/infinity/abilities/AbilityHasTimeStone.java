package anvil.infinity.abilities;

import net.minecraft.entity.EntityLivingBase;

public class AbilityHasTimeStone extends AbilityHasStone {

    public AbilityHasTimeStone(EntityLivingBase entity) {
        super(entity);
    }
}
