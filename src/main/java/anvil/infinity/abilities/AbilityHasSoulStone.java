package anvil.infinity.abilities;

import net.minecraft.entity.EntityLivingBase;

public class AbilityHasSoulStone extends AbilityHasStone {

    public AbilityHasSoulStone(EntityLivingBase entity) {
        super(entity);
    }
}
