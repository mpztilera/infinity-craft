package anvil.infinity.abilities;

import lucraft.mods.lucraftcore.superpowers.abilities.AbilityConstant;
import net.minecraft.entity.EntityLivingBase;

public class AbilityHasStone extends AbilityConstant {

    public AbilityHasStone(EntityLivingBase entity) {
        super(entity);
    }

    @Override
    public void updateTick() {

    }
}
