package anvil.infinity.abilities;

import net.minecraft.entity.EntityLivingBase;

public class AbilityHasRealityStone extends AbilityHasStone {

    public AbilityHasRealityStone(EntityLivingBase entity) {
        super(entity);
    }
}
